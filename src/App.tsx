import { FormEvent, useState } from "react";
import "./App.css";
import RenderFee from "./components/RenderFee";
import { FormWrapper } from "./FormWrapper";
import { Values } from "./constants/Types";
import { calculateDeliveryFee } from "./constants/calculateDeliverFee";

const INITIAL_VALUES: Values = {
  cartValue: "",
  deliveryDistance: "",
  amountOfItems: "",
  time: "",
  fee: "",
};

function App() {
  const [values, setValues] = useState(INITIAL_VALUES);

  function onSubmit(e: FormEvent) {
    e.preventDefault();

    calculateDeliveryFee({ values, setValues });
  }

  return (
    <FormWrapper>
      <h1>Delivery Fee Calculator</h1>

      <form className="delivery-fee-calc" onSubmit={onSubmit}>
        <div className="field">
          <label>Cart Value (€)</label>
          <input
            required
            autoFocus
            value={values.cartValue}
            onChange={(e) =>
              setValues({ ...values, cartValue: e.target.value })
            }
            type="number"
            id="cart-value"
            step="0.01"
            min="0.01"
            placeholder="0.00"
          />
        </div>
        <div className="field">
          <label>Delivery Distance (m)</label>
          <input
            required
            value={values.deliveryDistance}
            onChange={(e) =>
              setValues({ ...values, deliveryDistance: e.target.value })
            }
            type="number"
            id="delivery-distance"
            min={1}
            placeholder="1500"
          />
        </div>
        <div className="field">
          <label>Amount of Items</label>
          <input
            required
            value={values.amountOfItems}
            onChange={(e) =>
              setValues({ ...values, amountOfItems: e.target.value })
            }
            type="number"
            id="amount-of-items"
            min={1}
            placeholder="0"
          />
        </div>
        <div className="field">
          <label>Date and Time</label>
          <input
            required
            value={values.time}
            onChange={(e) => setValues({ ...values, time: e.target.value })}
            type="datetime-local"
            id="time"
          />
        </div>
        <div className="field">
          <button>Calculate Delivery Fee</button>
        </div>
      </form>
      <RenderFee fee={values.fee} />
    </FormWrapper>
  );
}

export default App;
