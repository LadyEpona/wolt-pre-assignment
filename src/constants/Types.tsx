export type Values = {
  cartValue: string;
  deliveryDistance: string;
  amountOfItems: string;
  time: string;
  fee: string;
};

export type RenderFeeProps = {
  fee: number | string;
};

export type CalculationProps = {
  values: Values;
  setValues: Function;
};

export type FormWrapperProps = {
  children: JSX.Element | JSX.Element[];
};
