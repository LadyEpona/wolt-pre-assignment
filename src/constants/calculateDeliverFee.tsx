import { CalculationProps } from "./Types";

export const calculateDeliveryFee = ({
  values,
  setValues,
}: CalculationProps) => {
  const baseDistanceFee = 1;
  const deliveryIsFree = Number(values.cartValue) >= 100;
  const addSurchargeBasedOnCartValue = Number(values.cartValue) < 10;
  const increaseDistanceFeeForEvery500Meters =
    Number(values.deliveryDistance) > 500;
  const fridayValueInUTC = 5;
  const chosenDateInUTC = new Date(values.time).getUTCDay();
  const checkIfFriday = chosenDateInUTC === fridayValueInUTC;
  const chosenHoursInUTC = new Date(values.time).getUTCHours();
  const rushStartHour = 15;
  const rushEndHour = 19;
  const checkIfInRushHours =
    checkIfFriday &&
    chosenHoursInUTC >= rushStartHour &&
    chosenHoursInUTC < rushEndHour;
  const maxFee = 15;

  let surchargeBasedOnCartValue = 0;
  let distanceFee = baseDistanceFee;
  let surchargeBasedOnAmountOfItems = 0;

  if (deliveryIsFree) {
    return setValues({ ...values, fee: "0" });
  }
  if (addSurchargeBasedOnCartValue) {
    surchargeBasedOnCartValue = Number(
      (10 - Number(values.cartValue)).toFixed(2)
    );
  }
  if (increaseDistanceFeeForEvery500Meters) {
    distanceFee = Math.floor(Number(values.deliveryDistance) / 500) * 1;
  }
  if (Number(values.amountOfItems) >= 5) {
    const amountOfItemsFreeOfSurcharge = 4;
    const surchargePerItem = 0.5;
    const amountOfItemsForApplyingBulkSurcharge = 12;
    const bulkSurcharge =
      Number(values.amountOfItems) > amountOfItemsForApplyingBulkSurcharge
        ? 1.2
        : 0;
    surchargeBasedOnAmountOfItems =
      (Number(values.amountOfItems) - amountOfItemsFreeOfSurcharge) *
        surchargePerItem +
      bulkSurcharge;
  }
  if (checkIfInRushHours) {
    const rushHourMultiplier = 1.2;
    const calculateFee = Number(
      (
        (surchargeBasedOnCartValue +
          distanceFee +
          surchargeBasedOnAmountOfItems) *
        rushHourMultiplier
      ).toFixed(2)
    );
    const checkIsFeeOver15 = calculateFee > maxFee ? maxFee : calculateFee;
    return setValues({ ...values, fee: checkIsFeeOver15.toString() });
  }
  const calculateFee = Number(
    (
      surchargeBasedOnCartValue +
      distanceFee +
      surchargeBasedOnAmountOfItems
    ).toFixed(2)
  );
  const checkIsFeeOver15 = calculateFee > maxFee ? maxFee : calculateFee;
  return setValues({ ...values, fee: checkIsFeeOver15.toString() });
};
