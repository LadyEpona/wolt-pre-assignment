import { FormWrapperProps } from "./constants/Types";

export function FormWrapper({ children }: FormWrapperProps) {
  return (
    <>
      <div className="wrapper">{children}</div>
    </>
  );
}
