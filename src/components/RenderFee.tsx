import CountUp from "react-countup";
import { RenderFeeProps } from "../constants/Types";

const RenderFee = ({ fee }: RenderFeeProps) => {
  if (fee !== "") {
    return (
      <div id="calculated-fee">
        Delivery Fee: <CountUp end={Number(fee)} duration={0.8} decimals={2} />{" "}
        €
      </div>
    );
  } else {
    return <></>;
  }
};

export default RenderFee;
